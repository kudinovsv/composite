Vue.component('composite', {
    props: ['id', 'name', 'number', 'children'],
    template:
        `<div class="composite" v-on:click.stop="addChild"
                                v-on:contextmenu.stop.prevent="removeMe">
        {{ name }} {{ number }}
        <composite v-for="child in children" v-bind="child" :key="child.number"
                                             v-on:removeChild="removeChild">
        </composite>
        </div>`,
    data: function() {
  	return {
            number_counter: 0
        };
    },
    methods: {
  	addChild: function() {
            var params =  {
                name: this.number == undefined ? 'Child' : 'Sub' + this.name,
                number: this.getNewNumber(),
                parent: this.id
            };
      
            axios.get('/add', {params: params})
                .then(response => {
                    delete params.parent;
                    params.id = response.data.id;
                    params.children = [];
                    this.children.push(params);
                });                
        },
        removeMe: function() {
            this.$emit('removeChild', this.number);      
        },
        removeChild: function(number) {
            for (var i = 0; i < this.children.length; i++) {
                var child = this.children[i];
                if (child.number == number) {
                    axios.get(`/delete?id=${child.id}`)
                        .then(() => this.children.splice(i, 1)); 
                    break;
                }
            }
        },
        getNewNumber: function() {
            if (this.number_counter == 0) { 
                var max = 0;
                for (var child of this.children)
                    if (max < child.number)
                        max = child.number;
                this.number_counter = max + 1;  
            }
            return this.number_counter++;
        }
    }
});

var app = new Vue({
    el: '#app',
    data: {
        children: [],
        loaded: false
    },
    methods: {
        loadData: function() {
            axios.get('/getAll')
                .then(response => {
                    this.children = response.data;
                    this.loaded = true;
                });   
        }        
    }
});

axios.interceptors.response.use(null, function(error) {
    alert(error.message);
    return Promise.reject(error);
});

app.loadData();
