<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Composite extends Model
{
    public $timestamps = false;
    protected $fillable = ['name', 'number', 'parent'];
           
    public function children() {
        return $this->hasMany('App\Composite', 'parent');
    }
    
    private static function getChildren($children) {
        $res = [];
        foreach ($children as $child) {
            array_push($res, ['id' => $child->id,
                              'name' => $child->name,
                              'number' => $child->number,
                              'children' => self::getChildren($child->children)
                             ]);
        }
        return $res;
    }        
    
    public static function getAll() {
        return self::getChildren(self::where('parent', null)->get());
    }
}
