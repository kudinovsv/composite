<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Composite;
use Illuminate\Http\Request;

Route::get('/', function () {
    return view('composites');
});

Route::get('/getAll', function () {
    return Composite::getAll();
});

Route::get('/add', function (Request $request) {
    return ['id' => Composite::create($request->all())->id];
});

Route::get('/delete', function (Request $request) {
    Composite::destroy($request->input('id'));
});
