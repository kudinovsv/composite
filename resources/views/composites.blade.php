<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Composite Test Task</title>

    <script src="https://unpkg.com/vue@2.3.3/dist/vue.min.js"></script>
    <script src="https://unpkg.com/axios@0.16.1/dist/axios.min.js"></script>
    <script defer src="{{ URL::asset('js/app.js') }}"></script>
    
    <style>
        #app p {
            margin: 0px 0px 5px;
        }

        div.composite {
            border: 1px solid blue;
            border-radius: 5px;
            padding: 2px 6px;
        }        
    </style>
  </head>

  <body>    
    <div id="app">
      <p v-if="!loaded">
        Loading...
      </p>
      <template v-else>
        <p>Left click for add, right click for remove</p>
        <composite name='Root' :children='children'></composite>
      </template>
    </div>
  </body>  
</html>

